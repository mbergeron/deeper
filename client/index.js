import Vue from 'vue';
import VueRouter from 'vue-router';
import RethinkDB from './rethinkdb';
import App from './components/App.vue';
import CardList from './components/CardList.vue';
import CardView from './pages/CardView.vue';
import PlayGroundView from './pages/PlayGroundView.vue';
import Cursor from './pages/Cursor.vue';
import About from './pages/About.vue';
import RoomJoin from './pages/RoomJoin.vue';

// Global styles
import './assets/card.css';
import './assets/layout.css';


// Routes definitions
const router = new VueRouter({
  routes: [
    { path: '/', component: { template: '<p>Home</p>' } },
    { path: '/cards', component: CardList },
    { path: '/cards/:name', component: CardView }
    { path: '/about', component: About },
    { name: 'playground', path: '/playground/:roomId', component: PlayGroundView, props: true},
    { path: '/cursor', component: Cursor },
    { path: '/join', component: RoomJoin, props: (route) => route.query },
  ]
})

Vue.use(VueRouter)
Vue.use(RethinkDB)

// Vue bootstrap
new Vue({
  el: '#app',
  router,
  render: h => h(App),
})
