const RethinkdbWebsocketClient = require('rethinkdb-websocket-client');
export const r = RethinkdbWebsocketClient.rethinkdb;

var defaults = {
  host: process.env.SERVER_HOST || process.env.HOSTNAME || 'localhost', // hostname of the websocket server
  port: process.env.SERVER_PORT || 8025,           // port number of the websocket server
  path: '/.ws/rethinkdb',                          // HTTP path to websocket route
  wsProtocols: ['binary'],                         // sub-protocols for websocket, required for websockify
  wsBinary: 'arraybuffer',                         // specify which binary type should be used for WS (optional)
  secure: false,                                   // set true to use secure TLS websockets
  db: 'deeper',                                    // default database, passed to rethinkdb.connect
  //simulatedLatencyMs: 100,                       // wait 100ms before sending each message (optional)
};

export const db = {
  connect(options) {
    const connectOptions = Object.assign({}, defaults, options)
    return RethinkdbWebsocketClient.connect(connectOptions);
  }

  async run(query) {
    let conn = await this.connect()
    return query.run(conn)
                .finally(() => conn.close())
  }

  async changes(query) {
    let conn = await this.connect()
    return query.changes({includeInitial: true}).run(conn)
  }
}

export default {
  install(Vue) {
    // is this needed?
    Vue.$db = db
    Vue.prototype.$db = db
  }
}
