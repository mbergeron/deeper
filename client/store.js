import Vue from 'vue'
import _ from 'lodash'
import { r, db } from '~/rethinkdb.js'
import RethinkDB from '~/rethinkdb.js'
import uuid from 'uuid/v4'


Vue.use(RethinkDB)


const PIN_LENGTH = 4
const t = r.table

// Model, represent a RethinkDB feed
export class Room extends Vue {
  constructor(model) {
    super({
      data: {
        id: model.id,
        feed: rooms.table.get(model.id),
        propagate: true,
        model,
      },

      created() {
        this._oplog = this.model._oplog || 0
        this.$db.changes(this.feed).then(
          feed => feed.eachAsync(
            async changes => {
              if (!changes) return;

              this.propagate = false;
              this.model = changes.new_val

              this.$nextTick(function(vm) {
                this.propagate = true;
              });
            }
          )
        )
      },

      watch: {
        model: {
          async handler(model) {
            if (!this.propagate) return;

            // propagate this change
            await this.$db.run(rooms.table.update(this.model))
            console.log("Updated room!")
          },
          deep: true
        }
      },
    })
  },
}

export class GameState extends Vue {
  constructor(seat, gameState) {
    super({
      data: {
        seat,
        state: gameState
      },

      methods: {
        discard(card) {
          this.state.discard[card.owner].push(card)
        }
      }
    })
  }
}


export const cards = {
  table: t("cards"),
}

export const clients = new Vue({
  data: {
    currentId: null,
  },

  created() {
    this.currentId = window.sessionStorage.getItem("clientId") || uuid()
  },

  watch: {
    currentId(val) {
      window.sessionStorage.setItem("clientId", val)
    }
  }
})

export const rooms = new Vue({
  data: {
    table: t("rooms"),
    current: null,
    count: 0,
    all: {},
  },

  async created() {
    this.count = await this.$db.run(this.table.count())
    this.$db.changes(this.table).then(
      feed => feed.eachAsync(async (changes) => {
        if (!changes) return;

        this.$set(this.all, changes.new_val.id, changes.new_val)
      })
    )
  },

  methods: {
    generatePin() {
      let digits = _.times(PIN_LENGTH, () => _.random(0, 9))

      return _.join(digits, '')
    },

    async create(pin) {
      const attrs = {
        _oplog: 0,
        pin: pin || this.generatePin()
        seats: [],
        gameState: {
          stack: [],
          rules: [],
          discard: [[], []],
          library: [[], []],
          tmpLeft: [[], []],
          tmpRight: [[], []],
          score: [0, 0],
        }
      }

      let room = await db.run(
        this.table.insert(attrs, {returnChanges: true})
      )

      return new Room(room)
    },

    async enter(id) {
      const room = await db.run(this.table.get(id))

      this.current = new Room(room)
    },

    async tryJoin(id, pin, playerId) {
      let room = await db.run(this.table.get(id))
      let playerSeat = _.indexOf(room.seats, playerId)

      // already in the game
      if (playerSeat > -1)
        return playerSeat

      // game is full
      if (room.seats.length >= 2)
        return false;

      // player joins the game
      if (room.pin == pin) {
        room.seats.push(playerId)
        await db.run(this.table.update(room))
        return room.seats.length - 1
      }

      return false
    },
  }
})
