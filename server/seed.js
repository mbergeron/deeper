const r = require('rethinkdb');
const fs = require('fs');
const glob = require('glob');
const yaml = require('js-yaml');

const connect = r.connect({ host: process.env.RETHINKDB_HOST, db: 'deeper' })

const deleteSeeds = conn => r.table('cards')
                             .filter(card => card.hasFields('_src'))
                             .delete()
                             .run(conn)
                             .then(() => conn)

const seedCards = conn => {
  return new Promise((resolve, reject) => {
    glob('data/**/*.yaml', (err, files) => {
      const documents = files.map((file, i) => {
        const base = yaml.load(fs.readFileSync(file))
        base["id"] = i
        base["_src"] = file

        return base
      })

      return r.table('cards')
              .insert(documents)
              .run(conn)
              .catch(reject)
              .then(() => resolve(conn))
    })
  })
}

const seedRoom = conn => r.table('rooms')
                          .replace({
                            id: "_default",
                            score: [0, 0],
                            library: [[], []],
                            stack: [[], []],
                            cards: [[], []],
                          })
                          .run(conn)
                          .then(() => conn)

connect.then(deleteSeeds)
       .then(seedCards)
       .then(seedRoom)
       .then(conn => conn.close())
