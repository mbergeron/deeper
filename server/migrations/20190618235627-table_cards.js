'use strict'

exports.up = function (r, connection) {
  // must return a Promise!
  return r.tableCreate('cards').run(connection)
}

exports.down = function (r, connection) {
  // must return a Promise!
  return r.tableDrop('cards').run(connection)
}
