'use strict'

exports.up = function (r, connection) {
  // must return a Promise!
  return r.tableCreate('rooms').run(connection)
}

exports.down = function (r, connection) {
  // must return a Promise!
  return r.tableDrop('rooms').run(connection)
}
