'use strict'

exports.up = function (r, connection) {
  // must return a Promise!
  return r.tableCreate('players').run(connection)
}

exports.down = function (r, connection) {
  // must return a Promise!
  return r.tableDrop('players').run(connection)
}
