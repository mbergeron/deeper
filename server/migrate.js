module.exports = {
  db: "deeper",
  host: process.env.RETHINKDB_HOST,
  migrationsDirectory: "/usr/src/app/server/migrations",
  ssl: false
}
