var http = require('http');
var RethinkdbWebsocketServer = require('rethinkdb-websocket-server');
var express = require('express');

const RETHINKDB_HOST = process.env.RETHINKDB_HOST || 'localhost';
const RETHINKDB_PORT = process.env.RETHINKDB_PORT || 28015;
const SERVER_BIND_HOST = process.env.SERVER_BIND_HOST || '0.0.0.0';
const SERVER_BIND_PORT = process.env.SERVER_BIND_PORT || 8025;

var app = express();

// setup the default route for the SPA
app.use('/', express.static('public/'));

var httpServer = http.createServer(app);

RethinkdbWebsocketServer.listen({
  httpServer: httpServer,
  httpPath: '/.ws/rethinkdb',
  dbHost: RETHINKDB_HOST,
  dbPort: RETHINKDB_PORT,
  unsafelyAllowAnyQuery: true,
});

console.log("RethinkDB socket started.")
console.log(RETHINKDB_HOST)

httpServer.listen({
  host: SERVER_BIND_HOST,
  port: SERVER_BIND_PORT,
});
