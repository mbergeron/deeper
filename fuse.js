const {
  FuseBox,
  VueComponentPlugin,
  QuantumPlugin,
  HTMLPlugin,
  SassPlugin,
  CSSPlugin,
  CSSResourcePlugin,
  WebIndexPlugin,
  EnvPlugin,
  Sparky
} = require("fuse-box");

let fuse;
let isProduction = false;
let outputDir = "public/"

Sparky.task("set-prod", () => {
  isProduction = true;
});

Sparky.task("clean", () => Sparky.src("./dist")
                                 .clean(outputDir));

Sparky.task("copy-assets", () => {
  return Sparky.src("./assets/img/**/*.png", { base: "./client" })
               .dest(outputDir)
               .exec()
})

Sparky.task("config", () => {
  fuse = FuseBox.init({
    homeDir: "./client",
    target: "browser@es2017",
    output: `${outputDir}$name.js`,
    sourceMaps: !isProduction,
    alias: {
      vue: "vue/dist/vue.esm.js", // with template compiler
    },
    useTypescriptCompiler: true,
    allowSyntheticDefaultImports: true,
    plugins: [
      VueComponentPlugin({
        style: [
          SassPlugin({
            importer: true
          }),
          CSSResourcePlugin(),
          CSSPlugin({
            group: 'components.css',
            inject: 'components.css'
          })
        ]
      }),
      SassPlugin(),
      CSSPlugin(),
      EnvPlugin({
        HOSTNAME: process.env.HOSTNAME,
        SERVER_HOST: process.env.SERVER_HOST,
        SERVER_PORT: process.env.SERVER_PORT,
      }),
      /* isProduction && QuantumPlugin({
       *   bakeApiIntoBundle: "vendor",
       *   uglify: true,
       *   treeshake: true,
       * }), */
      WebIndexPlugin({
        template: './client/index.html'
      }),
    ]
  });

  if(!isProduction){
    fuse.dev({
      port: 8080,
    });
  }

  const vendor = fuse.bundle("vendor")
                     .instructions("~ index.js");

  const app = fuse.bundle("app")
                  .instructions("> [index.js]");

  if(!isProduction){
    app.watch().hmr();
  }
})

Sparky.task("default", ["clean", "copy-assets", "config"], () => {
  return fuse.run();
});

Sparky.task("dist", [ "clean", "set-prod", "copy-assets", "config"], () => {
  return fuse.run();
});
