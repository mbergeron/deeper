FROM node as base
ARG SERVER_HOST
ARG SERVER_PORT
WORKDIR /usr/src/app
RUN chown -R 1000:1000 .
USER 1000:1000
COPY . .
RUN npm install && npm run start dist
ENTRYPOINT [ "npm" ]


FROM node:alpine as server
ARG SERVER_BIND_HOST
ARG SERVER_BIND_PORT
EXPOSE 80
WORKDIR /usr/src/app
COPY package.json ./
COPY ./server ./server
COPY ./data ./data
COPY --from=base /usr/src/app/public/ ./public
RUN npm install --only=production
ENTRYPOINT [ "npm", "run" ]
CMD [ "api" ]


FROM base as client-dev
EXPOSE 8080
ENTRYPOINT [ "npm", "run", "start" ]

FROM base as server-dev
EXPOSE 8025
ENTRYPOINT [ "npm", "run" ]
CMD [ "api" ]
