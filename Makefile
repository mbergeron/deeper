.PHONY: build push deploy


DOCKER_REMOTE=--tls --tlscacert ${HOME}/.docker/ca.pem -H 104.168.32.134:2376

build:
	docker-compose -f docker-compose.prod.yml build server

push:
	docker push micaelbergeron/deeper

deploy:
	docker ${DOCKER_REMOTE} pull micaelbergeron/deeper
	docker-compose -f docker-compose.prod.yml ${DOCKER_REMOTE} up -d server

.PHONY: db server seed migrate


db:
	docker-compose up -d db

server: db
	docker-compose up -d server

migrate:
	docker-compose exec server npm run migrate up

seed: migrate
	docker-compose exec server node server/seed.js
