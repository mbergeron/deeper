---
name: Lucky Dice
cost: 1
worth: 
card_type: Action
art: sample.png
draft: false
author: Ben Hong
rules:
- S: -X points, where X is the cost of the next card
- R: +X*2 points, where X is the cost of the next card
art:
  sdxl:
    version: beta
    prompt: >
      People cheering around a table where two dices are showing six dots, in the background people are drinking on wooden stools and tables,

      fantasy, medieval, aquarel