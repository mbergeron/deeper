---
name: Jewel
cost: 1
worth: 
card_type: Ore - Gem 
art: sample.png
draft: false
rules:
- S: +2 points if the next card is either `Gold` or `Silver`
- R: +2 points if the next card is a `Gem`
